document.addEventListener("DOMContentLoaded", function(event) { 

    editor = document.getElementById("actual-editor");
    addLink = document.getElementById("article-add-link");
    selected = false;
    
    // Adding HTML elements
    function wrapSelectedText(node, options) {
        
        // just a catch
        if(typeof options === "undefined") {
            options = null;
        }

        var selection= window.getSelection().getRangeAt(0);
        var selectedText = selection.extractContents();
        var styled= document.createElement(node);

        styled.href = options;
        styled.target = "_blank";
        styled.className = "edielink";

        styled.appendChild(selectedText);
        selection.insertNode(styled);
    }

    // Clearing the whole text of any HTML elements/formatting
    function strip() {
        var tmp = document.createElement("div");
        tmp.innerHTML = editor.innerHTML;
        editor.innerHTML = tmp.textContent||tmp.innerText;
     }



    document.addEventListener("click", function(e) {
        if (e.target.id === "article-clear") {strip()}

        if (e.target.id === "article-typo-bold") {
            wrapSelectedText("b");
        }
        if (e.target.id === "article-typo-italic") {
            wrapSelectedText("i");
        }
        if (e.target.id === "article-typo-heading") {
            wrapSelectedText("h3");
        }
        if (e.target.id === "blockquote") {
            wrapSelectedText("blockquote");
        }

    });

  });